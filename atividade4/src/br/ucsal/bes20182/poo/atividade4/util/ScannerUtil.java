package br.ucsal.bes20182.poo.atividade4.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class ScannerUtil {
	
	public static Scanner scanner = new Scanner(System.in);
	
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	public static Integer obterInteiro(String mensagem) {
		System.out.println(mensagem);
		Integer numero = scanner.nextInt();
		scanner.nextLine();
		return numero;
	}

	public static Date obterData(String mensagem) {
		while (true) {
			System.out.println(mensagem);
			String dataString = scanner.nextLine();
			try {
				Date data = sdf.parse(dataString);
				return data;
			} catch (ParseException e) {
				System.out.println("A data informada n�o � v�lida.");
			}
		}
	}
	
	public static String formatarData(Date data){
		return sdf.format(data);
	}

}
