package br.ucsal.bes20182.lista11.enums;

public enum TipoCarga {
	
	LIQUIDO(1), GAS(2), SOLIDOS(3);

	private int valor;

	private TipoCarga(int valor) {
		this.valor = valor;
	}

	public int getValue() {
		return valor;
	}
}
