package br.ucsal.bes20182.lista11.enums;

public enum TipoMoto {
	
	CAMPO(1),CIDADE(2);
	
	private int valor;

	private TipoMoto(int valor) {
		this.valor = valor;
	}

	public int getValue() {
		return valor;
	}
}

