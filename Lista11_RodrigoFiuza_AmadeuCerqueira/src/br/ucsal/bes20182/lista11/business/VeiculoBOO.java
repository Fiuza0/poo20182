package br.ucsal.bes20182.lista11.business;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20182.lista11.dto.*;

public class VeiculoBOO {
	public static List<VeiculoDTO> veiculos = new ArrayList<VeiculoDTO>();

	public static void Incluir(String placa, Integer anoFab, double valorVeiculo, VeiculoDTO veiculo) {
		veiculo.setPlaca(placa);
		veiculo.setValor(valorVeiculo);
		veiculo.setAnoFabricacao(anoFab);
		veiculos.add(veiculo);
	}

	public static void Listar(List<VeiculoDTO> Veiculos) {
		for (VeiculoDTO veiculo : Veiculos) {
			System.out.println("Placa: " + veiculo.getPlaca() + "\nValor: R$ " + veiculo.getValor()
					+ "\nAno de Fabricação: " + veiculo.getAnoFabricacao());
		}
	}

}
