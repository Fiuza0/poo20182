package br.ucsal.bes20182.poo.aula06.poc.list;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListExemplo1 {

	public static void main(String[] args) {
		List<Object> objetos = new ArrayList<>();

		objetos.add("Claudio");
		objetos.add(123);
		objetos.add(new Date());

		System.out.println("Quantidade de itens na lista: " + objetos.size());
		System.out.println("Segundo objeto = " + objetos.get(1));
		System.out.println("Existe o objeto 123?" + objetos.contains(123));
		System.out.println("Existe o objeto \"Caju\"?" + objetos.contains("Caju"));
		objetos.remove("Claudio");
		System.out.println("objetos=" + objetos);
		objetos.clear();
		System.out.println("Quantidade de itens na lista: " + objetos.size());
	}

}
