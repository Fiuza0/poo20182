package br.ucsal.bes20182.poo.aula08.poc.instanciaeclasse;

public class Aluno {

	private static Integer contador = 0;
	
	private String nome;

	private Integer matricula = 0;

	public Aluno(String nome) {
		super();
		this.nome = nome;
		
		contador++;
		this.matricula = contador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

}
